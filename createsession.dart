import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'Global.dart' as global;
import 'package:firebase_core/firebase_core.dart';

class CreateSession extends StatefulWidget {
  const CreateSession({ Key? key }) : super(key: key);

  @override
  State<CreateSession> createState() => _CreateSession();
}

class _CreateSession extends State<CreateSession> {
  @override
  Widget build(BuildContext context) {
    String DomumentID= 'unValue';
    const String title = "Mat App";
    final ref = FirebaseFirestore.instance.collection("Session").doc();

    TextEditingController fieldController1 = TextEditingController();
    TextEditingController fieldController2 = TextEditingController();
    TextEditingController fieldController3 = TextEditingController();

    Future _addSessions()
    {
      final fieldvalue1 = fieldController1.text;
      final fieldvalue2 = fieldController2.text;
      final fieldvalue3 = fieldController3.text;

      DomumentID = ref.id;
      return ref
          .set({"field1" : fieldvalue1, "field2": fieldvalue2,"field3": fieldvalue3, "doc_id": ref.id})
          .then((value) => log("Collection added"))
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text(title),
    ),
        body: Padding(
                padding: const EdgeInsets.symmetric(
                horizontal: 100,
                vertical: 8),
            child: Column(
              children: [
                const SizedBox(
                  width:  110.0,
                  height: 110.0,
                ),
                TextField(
                  controller: fieldController1,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(120),
                      ),
                      hintText: "Enter Subject name"
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  controller: fieldController2,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(120),
                      ),
                      hintText: "Enter Subject name"
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  controller: fieldController3,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(120),
                      ),
                      hintText: "Enter Subject name"
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                    onPressed: (){
                      _addSessions();
                      global.docID = ref.id;
                      },
                    child: const Text("Submit")

                )
              ],
            )

        )
    );

  }
}