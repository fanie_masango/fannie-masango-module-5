import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'createsession.dart';
import 'dataread.dart';
import 'Global.dart' as global;


Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyCxF9SEFTE1AgmnJ5sQx82t1ciYw7vNArc",
          authDomain: "mtnapp-f8da3.firebaseapp.com",
          projectId: "mtnapp-f8da3",
          storageBucket: "mtnapp-f8da3.appspot.com",
          messagingSenderId: "690659591444",
          appId: "1:690659591444:web:d71f5112a84e194baaee01",
          measurementId: "G-W8M7F4T6PC"
      )
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final ref = FirebaseFirestore.instance.collection("Session").doc();
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none ,
                children: const [
                ],
              ),
            ),
             Padding(
              padding:  const EdgeInsets.all(8.0),
              child:  ElevatedButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const CreateSession()),);
                  },
                  child: const Text("Create data"),
              ),
            ),
            Padding(
              padding:  const EdgeInsets.all(8.0),
              child:  ElevatedButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const DataRead()),);
                },
              ),
            ),
            Padding(
              padding:  const EdgeInsets.all(8.0),
              child:  ElevatedButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const DataRead()),);
                },
                child: const Text("Read Data"),
              ),
            ),
          ]
        ),
      ),
    );
  }
}
