import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'Global.dart' as global;
import 'getItem.dart';
import 'package:firebase_core/firebase_core.dart';

class DataRead extends StatefulWidget {
  const DataRead({ Key? key }) : super(key: key);

  @override
  State<DataRead> createState() => _DataRead();
}

class _DataRead extends State<DataRead> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: const Text('Data Read'),
        ),
        body: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: 100,
                vertical: 8),
            child:Center(
              child: Column(
                children:   [
                  SizedBox(
                    width:  110.0,
                    height: 110.0,
                  ),
                  Text("Received Data",
                    style:  TextStyle(
                      color: Colors.red,
                      fontSize: 50.0,
                    ),
                  ) ,
                  SizedBox(
                    height: 10,
                  ),
                   GetItem(global.docID),
                ],
              ),
            )
        )
    );

  }
}