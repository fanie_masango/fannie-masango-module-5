import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class GetItem extends StatelessWidget {
  final String documentId;

    const GetItem(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference users = FirebaseFirestore.instance.collection('Session');

    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {

        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist at: " + documentId );
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data!.data() as Map<String, dynamic>;
          return
            ListView.separated(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
            padding: const EdgeInsets.all(8),
            itemCount: data.length,

            itemBuilder: (BuildContext context, int index) {
                String v = '-';
                switch(index)
                {
                  case 0:
                    v = 'field1';
                    break;
                  case 1:
                    v = 'field2';
                    break;
                  case 2:
                    v = 'field3';
                }
                if(index == 3)
                  return const Text('');
              return Container(
                height: 50,
                color: Colors.greenAccent,
                child: Center(child: Text(' ${data[v]}')),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          );
        }
        return Text("loading");
      },
    );
  }
}